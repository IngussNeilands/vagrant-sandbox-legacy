require 'json'

def get_JSON(file)

    begin

        return File.exists?(file)? JSON.parse(File.read(file), symbolize_names: true) : {}

    rescue JSON::ParserError => e

        return {}

    end

end
