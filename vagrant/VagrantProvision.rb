def provision( vagrantCMD, server, vmServer )

    case vagrantCMD

    when "up"
        if File.exists?( ".vagrant/machines/#{vmServer[:"id"]}/virtualbox/action_provision" )
            provision_shell( server, vmServer[:"vm.provision.up"] )
        else
            if vmServer[:"vm.provision.install.ansible"] == true then
                server.vm.provision 'shell', path: "ansible/setup/setup.sh", run: "always"
            end
            provision_shell( server, vmServer[:"vm.provision.install"] )
        end

    when "provision"
        provision_shell( server, vmServer[:"vm.provision"] )

    end

end

def provision_shell( server, provisionScripts )
    if Hash == provisionScripts.class then
        provisionScripts.each do |scriptFile, scriptArgs|
            if File.exists?( "#{scriptFile}" ) then 
                scriptArgs = Hash == scriptArgs.class ? scriptArgs : {}
                if scriptFile.to_s.end_with?("yml") then
                    server.vm.provision 'shell', inline: "ansible-playbook -i 127.0.0.1, /vagrant/#{scriptFile} --extra-vars '" + scriptArgs.to_json + "'", run: "always"
                else
                    server.vm.provision 'shell', path: "#{scriptFile}", run: "always"
                end
            end
        end
    end
end
