require_relative "get_json.rb"

class VagrantServerClass

    @@vagrantServer = {}

    @@defaultConfig = {}
    @@defaultServer = {}
    @@defaultBox = {}

    @@localConfig = {}

    def initialize()

        @@defaultConfig = get_JSON('vagrant/default/config.json')
        @@defaultServer = get_JSON('vagrant/default/vagrant.server.json')
        @@defaultBox = get_JSON('vagrant/default/vagrant.server.box.json')

        @@localConfig = get_JSON('vagrant/default/sample.Local.json')
        localConfigSource = get_JSON('Local.json')

        if localConfigSource[:"merge.server"] != nil then
            @@localConfig[:"merge.server"] = localConfigSource[:"merge.server"]
        end

        if @@localConfig[:"merge.server"] == true then
            @@defaultServer.each do |vmID, vmServer|
                vmServer = mergeServerConfig( vmServer, @@defaultConfig)
                @@vagrantServer[vmID] = getServer( vmID, vmServer )
            end
        end

        if localConfigSource.empty? == false then
            @@localConfig.each do |config, configHash|
                if Hash == localConfigSource[config].class then
                    @@localConfig[config] = localConfigSource[config]
                end
            end
            @@localConfig[:server].each do |vmID, vmServer|
                vmServer = mergeServerConfig( vmServer, @@localConfig[:config])
                vmServer = mergeServerConfig( vmServer, @@defaultConfig)
                @@vagrantServer[vmID] = getServer( vmID, vmServer )
            end
        end

        Dir.glob("**/*Sandbox.json", File::FNM_CASEFOLD) do |sandboxJson|
            sandboxConfig = get_JSON(sandboxJson)
            if Hash == sandboxConfig[:server].class then
                sandboxConfig[:server].each do |vmID, vmServer|
                    vmServer = mergeServerConfig( vmServer, sandboxConfig[:config])
                    vmServer = mergeServerConfig( vmServer, @@defaultConfig)
                    @@vagrantServer[vmID] = getServer( vmID, vmServer )
                end
            end
        end

        #puts JSON.pretty_generate( @@vagrantServer )
        #exit()

    end

    def mergeServerConfig( vmServer, vmServerConfig )
        if Hash == vmServerConfig.class then
            vmServerConfig.each do |config, configHash|
                vmServer[config] = vmServer[config] ||= configHash
            end
        end
        return vmServer
    end

    def getServer( vmID, vmServer )

        vmServer[:"id"] = vmID

        vmServer[:"box"] = vmServer[:"box"] ||= @@defaultBox.keys.first.to_s
        if @@defaultBox.key?(vmServer[:"box"].to_sym) then vmServer[:"box_url"] = @@defaultBox[vmServer[:"box"].to_sym].to_s end

        vmServer[:"hostname"] = vmServer[:"hostname"] ||= 'Sandbox' + vmID.slice(0,1).capitalize + vmID.slice(1..-1)

        return vmServer
    end

    def getVagrantServer()
        return @@vagrantServer
    end

end
