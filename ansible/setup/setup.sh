#!/bin/bash

if [ -f /etc/debian_version ] ; then

    codename=`lsb_release -c | awk '{print $2}'`

    apt-get update

    case $codename in
        'precise')
            apt-get install python-software-properties -y
            ;;
        *)
            apt-get install software-properties-common -y
    esac

    add-apt-repository ppa:ansible/ansible -y
    apt-get update

    apt-get install python-pip ansible -y
    #apt-get install --only-upgrade ansible -y

elif [ -f /etc/redhat-release ] ; then

    yum install epel-release -y

    yum install ansible -y

fi

cp -fr /vagrant/ansible/setup/ansible.cfg /etc/ansible/ansible.cfg
